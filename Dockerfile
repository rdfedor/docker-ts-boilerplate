FROM node:14-alpine

RUN apk add --update --virtual \
    build-base \
    curl \
    libc-dev \
    g++ \
    make \
    python-dev \
    python3 \
    sqlite \
    sqlite-dev \
    yarn

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY [package.json, yarn.lock] ./

RUN yarn install

COPY . .

EXPOSE 8080

CMD ["npm", "run", "migrate-start"]
