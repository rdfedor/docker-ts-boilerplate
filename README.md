# Docker Typescript Boilerplate

## Features

 * Model-Controller-Service folder structure with decorator based routing
 * Allows for creations and validation of JSON Web Key Sets
 * Role based user authentication and controller routing
 * Global error handling middleware
 * Dependency injection
 * Password hashing through bcrypt

## Folder Locations of Importance

 * __ tests __ - Folder containing the jest unit tests for the project.
 * migrations - Run once migrations
 * src/controller - Definition of controllers with route based decorators
 * src/database/sproc - Run-always scripts which is executed on start-up
 * src/model/requests - Request models used to validate incoming requests from the user