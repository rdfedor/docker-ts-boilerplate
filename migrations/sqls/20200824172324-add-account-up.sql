CREATE TABLE accounts(
    account_uuid BINARY(16) NOT NULL PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL DEFAULT '',
    last_name VARCHAR(100) NOT NULL DEFAULT '',
    password TEXT NOT NULL,
    email VARCHAR(254) NOT NULL,
    verified_email TINYINT(1) NOT NULL DEFAULT 0,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT email_unique UNIQUE(email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

