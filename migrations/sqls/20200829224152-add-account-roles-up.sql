CREATE TABLE account_roles(
  account_uuid BINARY(16) NOT NULL,
  role_name VARCHAR(100) NOT NULL DEFAULT '',
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT uc_account_roles_account_uuid_role_name UNIQUE(account_uuid, role_name),
  CONSTRAINT fk_account_role_account_uuid
    FOREIGN KEY (account_uuid)
    REFERENCES accounts (uuid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
