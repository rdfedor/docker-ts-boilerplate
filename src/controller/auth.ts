import {
  JsonController,
  Post,
  ContentType,
  Body,
  Put,
  ForbiddenError,
} from 'routing-controllers'
import { plainToClass } from 'class-transformer'
import { CreateAccountRequestModel } from 'src/model/request/account'
import { AuthenticateUserRequest } from 'src/model/request/auth'
import {
  SendPasswordResetRequest,
  ResetPasswordRequest,
} from 'src/model/request/password-reset'
import {
  SendEmailVerificationRequest,
  EmailVerificationRequest,
} from 'src/model/request/email-verification'
import { LoginTokenInterface } from 'src/interface/login-token'
import AuthService from '../services/auth'
import { RequestResult } from 'src/interface/request-result'
import { RefreshTokenRequest } from 'src/model/request/auth'
import { ValidationError } from 'class-validator'
import { NotFoundError, AccessDeniedError } from 'src/error'

@JsonController('/api/auth')
export default class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/')
  @ContentType('application/json')
  public async authenticateUser(
    @Body() req: { email: string; password: string },
  ): Promise<LoginTokenInterface> {
    try {
      return await this.authService.processAuthenticateUserRequest(
        plainToClass(AuthenticateUserRequest, req),
      )
    } catch (err) {
      if (
        err instanceof AccessDeniedError ||
        err instanceof NotFoundError ||
        (err instanceof Array &&
          err.filter((er) => er instanceof ValidationError).length > 0)
      ) {
        throw new ForbiddenError('Invalid email and/or password.')
      }
      throw err
    }
  }

  @Post('/register')
  @ContentType('application/text')
  public async registerUser(
    @Body()
    user: {
      firstName: string
      lastName: string
      password: string
      token: string
    },
  ): Promise<LoginTokenInterface> {
    return await this.authService.processRegisterAccount(
      plainToClass(CreateAccountRequestModel, user),
    )
  }

  @Post('/refresh')
  public async refreshToken(
    @Body() req: { token: string },
  ): Promise<LoginTokenInterface> {
    return await this.authService.processRefreshToken(
      plainToClass(RefreshTokenRequest, {
        token: req.token,
      }),
    )
  }

  @Post('/reset-password')
  @ContentType('application/text')
  public async sendPasswordResetRequest(
    @Body() req: { email: string },
  ): Promise<string> {
    return await this.authService.processSendPasswordResetRequest(
      plainToClass(SendPasswordResetRequest, req),
    )
  }

  @Put('/reset-password')
  @ContentType('application/json')
  public async verifyResetPassword(
    @Body() req: { token: string; password: string },
  ): Promise<RequestResult> {
    return await this.authService.processResetPasswordRequest(
      plainToClass(ResetPasswordRequest, req),
    )
  }

  @Post('/verify-email')
  @ContentType('application/text')
  public async sendVerificationEmail(
    @Body() body: { email: string },
  ): Promise<string> {
    return await this.authService.processSendEmailVerificationRequest(
      plainToClass(SendEmailVerificationRequest, {
        ...body,
      }),
    )
  }

  @Put('/verify-email')
  @ContentType('application/json')
  public async verifyForgotPassword(
    @Body() body: { challenge_code: string; token: string },
  ): Promise<RequestResult> {
    return this.authService.processEmailVerificationRequest(
      plainToClass(EmailVerificationRequest, body),
    )
  }
}
