import { JsonController, Get, ContentType } from 'routing-controllers'
import { JwtService } from 'src/services/jwt'
import { JSONWebKeySet } from 'jose'

@JsonController('/.well-known')
export default class JsonWebKeySetController {
  constructor(private jwtService: JwtService) {}

  @Get('/jwks.json')
  @ContentType('application/json')
  public async getJsonWebKeySet(): Promise<JSONWebKeySet> {
    return this.jwtService.getJsonWebKeySet()
  }
}
