import { JsonController, Get, ContentType } from 'routing-controllers'
import Database from 'src/database/index'

@JsonController('/api/status')
export default class StatusRoute {

  constructor(private db : Database) {}
  /**
   * @returns {{ status: boolean }}
   * @memberof StatusRoute
   */
  @Get('/')
  @ContentType('application/json')
  public async getIndex(): Promise<{ status: boolean, database: boolean, }> {
    let dbStatus = false
    try{
      await this.db.ping()
      dbStatus = true
    } catch (err) {
      console.log(err)
    }
    return {
      status: dbStatus,
      database: dbStatus,
    }
  }
}
