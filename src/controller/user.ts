import {
  JsonController,
  Get,
  ContentType,
  Param,
  Delete,
  Authorized,
  Req,
  Body,
  Post,
} from 'routing-controllers'
import AccountService from 'src/services/account'
import { plainToClass } from 'class-transformer'
import {
  GetAccountRequestModel,
  DeleteAccountRequestModel,
  AccountRoleRequest,
} from 'src/model/request/account'
import { AccountInterface } from 'src/interface/account'
import { JwtRequestInterface } from 'src/interface/jwt-request'
import { ROLE_ADMIN } from 'src/constant'
import { RequestResult } from 'src/interface/request-result'

@JsonController('/api/user')
export default class UserController {
  constructor(private accountService: AccountService) {}

  /**
   * @returns {Promise<AccountInterface[]>}
   * @memberof UserController
   */
  @Authorized()
  @Get('s')
  @ContentType('application/json')
  public async getUsers(): Promise<AccountInterface[]> {
    return await this.accountService.getAccounts()
  }

  /**
   * @returns {Promise<unknown>}
   * @memberof UserController
   */
  @Authorized()
  @Get('/')
  @ContentType('application/json')
  public async getCurrentUser(
    @Req() req: JwtRequestInterface,
  ): Promise<AccountInterface> {
    return await this.accountService.getAccount(
      plainToClass(GetAccountRequestModel, {
        account_uuid: req.token.account_uuid,
      }),
    )
  }

  @Authorized([ROLE_ADMIN])
  @Get('/:account_uuid')
  @ContentType('application/json')
  public async getUser(
    @Param('account_uuid') account_uuid: string,
  ): Promise<AccountInterface> {
    return await this.accountService.getAccount(
      plainToClass(GetAccountRequestModel, {
        account_uuid,
      }),
    )
  }

  @Authorized([ROLE_ADMIN])
  @Delete('/:uuid')
  @ContentType('application/json')
  public async deleteUser(
    @Param('account_uuid') account_uuid: string,
  ): Promise<RequestResult> {
    return await this.accountService.deleteAccount(
      plainToClass(DeleteAccountRequestModel, { account_uuid }),
    )
  }

  @Authorized()
  @Delete('/')
  @ContentType('application/json')
  public async deleteCurrentUser(
    @Req() req: JwtRequestInterface,
  ): Promise<RequestResult> {
    return await this.accountService.deleteAccount(
      plainToClass(DeleteAccountRequestModel, {
        account_uuid: req.token.account_uuid,
      }),
    )
  }

  @Authorized([ROLE_ADMIN])
  @Post('/:account_uuid/role')
  public async addUserRole(
    @Param('account_uuid') account_uuid: string,
    @Body() body: { role_name: string },
  ): Promise<RequestResult> {
    const { role_name } = body
    return await this.accountService.addAccountRole(
      plainToClass(AccountRoleRequest, {
        role_name,
        account_uuid,
      }),
    )
  }

  @Authorized([ROLE_ADMIN])
  @Delete('/:account_uuid/role')
  public async delUserRole(
    @Param('account_uuid') account_uuid: string,
    @Body() body: { role_name: string },
  ): Promise<RequestResult> {
    const { role_name } = body
    return await this.accountService.delAccountRole(
      plainToClass(AccountRoleRequest, {
        role_name,
        account_uuid,
      }),
    )
  }
}
