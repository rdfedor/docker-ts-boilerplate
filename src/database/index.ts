import {
  createPool,
  Pool,
  PoolConnection,
  UpsertResult,
} from 'mariadb'
import { glob } from 'glob'
import { promisify } from 'util'
import { readFile } from 'fs'
import * as Debug from 'debug'
import { Service } from 'typedi'
import {
  DATABASE_HOST,
  DATABASE_USER,
  DATABASE_PASS,
  DATABASE_NAME,
} from 'src/config'
import { QueryResultInterface } from 'src/interface/query-result'

const debug = Debug('appservice:database')

const pGlob = promisify(glob)
const pReadFile = promisify(readFile)

@Service()
export default class Database {
  private connectionPool: Pool

  constructor() {
    const profile = {
      host: DATABASE_HOST,
      user: DATABASE_USER,
      password: DATABASE_PASS,
      database: DATABASE_NAME,
      connectionLimit: 5,
      multipleStatements: true,
    }

    debug(`Connecting to database ${JSON.stringify(profile)}`)

    this.connectionPool = createPool(profile)
  }

  public async batch(
    query: string,
    parameters: Array<Array<string | number>>,
    connection: PoolConnection = null,
  ): Promise<UpsertResult[]> {
    const selectedConnection = await this.getConnection(connection)
    return await selectedConnection.batch(query, parameters)
  }

  /**
   * @returns {Promise<PoolConnection>}
   */
  public async beginTransaction(): Promise<PoolConnection> {
    const connection = await this.getConnection()
    await connection.beginTransaction()
    return connection
  }

  /**
   * @param {PoolConnection} connection
   * @returns {Promise<void>}
   */
  public async commit(connection: PoolConnection): Promise<void> {
    await connection.commit()
    await connection.release()
  }

  /**
   * @param {string} query
   * @param {(Array<string | number>)} parameters
   * @param {PoolConnection} [connection=null]
   * @returns {Promise<UpsertResult[]>}
   */
  public async query(
    query: string,
    parameters: Array<string | number> = null,
    connection: PoolConnection = null,
  ): Promise<QueryResultInterface> {
    const conn = await this.getConnection(connection)
    debug({
      query: query,
    })
    const rawResult = await conn.query(query, parameters)
    let data = null
    let meta = null
    let status = null

    if (rawResult instanceof Array) {
      let resultSet
      [resultSet, status] = rawResult
      meta = resultSet['meta']

      if (resultSet.length > 0) {
        data = resultSet.slice(0, resultSet.length)
      }
    } else {
      status = rawResult
    }

    const result: QueryResultInterface = {
      data,
      meta,
      status,
    }

    if (!connection) {
      conn.release()
    }

    return result
  }

  public async synchronizeStoredProcedures(): Promise<void> {
    debug('synchronizeStoredProcedures begin')
    const files = await pGlob('./src/database/sproc/*.sql')
    const conn: PoolConnection = await this.beginTransaction()
    try {
      files.forEach(async (file) => {
        debug(`Running ${file}`)
        const query = await pReadFile(file)
        await conn.query(query.toString())
      })
      await conn.commit()
    } catch (err) {
      debug('synchronizeStoredProcedures failed. rolling back...', err)
      await conn.rollback()
      throw err
    }
    debug('synchronizeStoredProcedures finish')
  }

  public async end(): Promise<void> {
    this.connectionPool.end()
  }

  public async ping(): Promise<void> {
    return (await this.getConnection()).ping()
  }

  /**
   * @private
   * @param {PoolConnection} [connection=null]
   * @returns {Promise<PoolConnection>}
   */
  private async getConnection(
    connection: PoolConnection = null,
  ): Promise<PoolConnection> {
    let selectedConnection: PoolConnection = connection

    if (!selectedConnection) {
      selectedConnection = await this.connectionPool.getConnection()
    }
    return selectedConnection
  }
}
