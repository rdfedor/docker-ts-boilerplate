CREATE OR REPLACE PROCEDURE addAccountRole(
    IN p_uuid VARCHAR(64),
    IN p_rolename VARCHAR(100)
  )
INSERT INTO account_roles (account_uuid, role_name)
VALUES (UuidToBin(p_uuid), p_rolename);
