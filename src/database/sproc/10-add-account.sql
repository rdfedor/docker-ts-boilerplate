CREATE OR REPLACE PROCEDURE addAccount(
  IN p_firstName VARCHAR(100)
  , IN p_lastName VARCHAR(100)
  , IN p_password TEXT
  , IN p_email VARCHAR(254)
)
BEGIN
  START TRANSACTION;
  SET @UUID := UUID();
  INSERT INTO accounts (
    account_uuid
    , first_name
    , last_name
    , password
    , email
    , verified_email
  ) VALUES (
    UuidToBin(@UUID)
    , p_firstName
    , p_lastName
    , p_password
    , p_email
    , true
  );
  COMMIT;
  SELECT @UUID as uuid;
END
