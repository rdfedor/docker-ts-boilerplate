CREATE OR REPLACE PROCEDURE delAccountRole(IN p_uuid VARCHAR(64), IN p_rolename VARCHAR(100))
DELETE FROM account_roles
WHERE account_uuid = UuidToBin(p_uuid) AND role_name = p_rolename;
