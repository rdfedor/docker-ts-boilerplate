CREATE OR REPLACE PROCEDURE getAccountByEmail(IN p_email varchar(254))
SELECT
  UuidFromBin(account_uuid) as account_uuid,
  first_name,
  last_name,
  password,
  email,
  verified_email,
  GROUP_CONCAT(role_name) as roles
FROM accounts
LEFT JOIN account_roles using (account_uuid)
WHERE email = p_email;
