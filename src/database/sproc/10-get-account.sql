CREATE OR REPLACE PROCEDURE getAccount(IN p_uuid varchar(36))
SELECT
  UuidFromBin(account_uuid) as account_uuid,
  first_name,
  last_name,
  email,
  verified_email,
  GROUP_CONCAT(role_name) as roles
FROM accounts
LEFT JOIN account_roles using (account_uuid)
WHERE account_uuid = UuidToBin(p_uuid);
