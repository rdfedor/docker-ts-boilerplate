CREATE OR REPLACE PROCEDURE getAccounts()
SELECT
  UuidFromBin(account_uuid) as account_uuid,
  first_name,
  last_name,
  email,
  verified_email,
  created_at
FROM accounts;
