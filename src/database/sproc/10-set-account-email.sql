CREATE OR REPLACE PROCEDURE setAccountEmail(
  IN p_uuid VARCHAR(64),
  IN p_email VARCHAR(254)
)
UPDATE accounts
SET email = p_email, verified_email = 1
WHERE account_uuid = UuidToBin(p_uuid) AND email != p_email
