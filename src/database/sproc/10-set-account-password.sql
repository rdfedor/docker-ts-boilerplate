CREATE OR REPLACE PROCEDURE setAccountPassword(
  IN p_email VARCHAR(254),
  IN p_password TEXT
)
UPDATE accounts SET password = p_password WHERE email = p_email
