export interface ErrorInterface {
  message: string
  httpCode: number
}

export const MissingParameterError = class MissingParameterError extends Error
  implements ErrorInterface {
  httpCode = 400
}
export const InvalidParameterError = class InvalidParameterError extends Error
  implements ErrorInterface {
  httpCode = 400
}
export const AccessDeniedError = class AccessDeniedError extends Error
  implements ErrorInterface {
  httpCode = 403
}
export const NotFoundError = class NotFoundError extends Error
  implements ErrorInterface {
  httpCode = 404
}
export const ConflictError = class ConflictError extends Error
  implements ErrorInterface {
  httpCode = 409
}
export const BadRequestError = class BadRequestError extends Error
  implements ErrorInterface {
  httpCode = 400
}
