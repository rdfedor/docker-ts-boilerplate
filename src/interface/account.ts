export interface AccountInterface {
  account_uuid: string
  first_name: string
  last_name: string
  password: string
  email: string
  verified_email: boolean
  roles: [string?]
}

export interface AccountQueryInterface {
  account_uuid: string
  first_name: string
  last_name: string
  password: string
  email: string
  verified_email: boolean
  roles: string
}
