export interface JsonWebKey {
  use: string,
  n: string,
  e: string,
}

export interface JsonWebKeySet {
  keys: [JsonWebKey]
}
