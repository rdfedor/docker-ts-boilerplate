import { RequestHandler } from "express";

export interface JwtToken {
  account_uuid: string
  email: string
  roles: [string]
}

export interface JwtRequestInterface extends RequestHandler {
  token: JwtToken
}
