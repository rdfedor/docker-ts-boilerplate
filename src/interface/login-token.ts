export interface LoginTokenInterface {
  token: string
  max_age?: string | number
  iat?: number
  roles: [string?]
  account_uuid: string
  email: string
  first_name?: string
  last_name?: string
  verified_email?: boolean
}
