export interface QueryResultInterface {
  data: [unknown]
  meta: [
    {
      columnLength: number
      columnType: number
      flags: number
      scale: number
      type: string
    },
  ]
  status: {
    affectedRows: number
    insertId: number
    warningStatus: number
  }
}
