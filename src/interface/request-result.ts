export interface RequestResult {
  status: number,
  message?: string
}
