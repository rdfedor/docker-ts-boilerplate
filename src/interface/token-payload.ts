export interface TokenPayloadInterface {
  account_uuid: string
  email: string
  roles: string[]
}
