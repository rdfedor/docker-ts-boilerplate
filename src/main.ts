import 'reflect-metadata' // this shim is required
import * as dotenv from 'dotenv'
dotenv.config()

import * as bodyParser from 'body-parser'
import * as compression from 'compression'
import * as express from 'express'
import * as expressRequestId from 'express-request-id'
import * as http from 'http'
import { useExpressServer, useContainer, Action } from 'routing-controllers'
import { Container } from 'typedi'

import { ENV, PORT } from './config'
import { ENVIRONMENT_PROD } from './constant'
import { TokenPayloadInterface } from './interface/token-payload'
import * as Database from './database/index'
import jwtService from './services/jwt'

console.log('Starting service...')

const app = express()
const server = http.createServer(app)

useContainer(Container)

app.disable('x-powered-by')

app.use(expressRequestId())
app.use(bodyParser.json())

console.log('Attaching controllers...')
useExpressServer(app, {
  authorizationChecker: async (action: Action, roles: string[]) => {
    if (action.request.headers['authorization']) {
      const [transport, token] = action.request.headers['authorization'].split(
        ' ',
      )

      if (transport.toLowerCase() === 'bearer') {
        const tokenPayload = <TokenPayloadInterface>jwtService.verify(token, {
          maxAge: '3h'
        })

        if (roles.length) {
          if (
            !tokenPayload.roles.filter((role) => roles.indexOf(role) !== -1)
              .length
          ) {
            return false
          }
        }

        action.request.token = tokenPayload
        return true
      }
    }

    return false
  },
  cors: true,
  defaultErrorHandler: false,
  controllers: [`${__dirname}/controller/**/*.ts`],
  middlewares: [`${__dirname}/middleware/**/*.ts`],
})

if (ENV === ENVIRONMENT_PROD) {
  app.use(compression())
}
console.log('Refreshing stored procedures')
const db = new Database.default()
db.synchronizeStoredProcedures().then(() => {
  db.end()

  server.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
  })
})
