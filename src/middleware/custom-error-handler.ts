import {
  Middleware,
  ExpressErrorMiddlewareInterface,
  ExpressMiddlewareInterface,
} from 'routing-controllers'
import { Request, Response, RequestHandler } from 'express'
import { ENV } from 'src/config'
import { ENVIRONMENT_PROD } from 'src/constant'
import { ValidationError } from 'class-validator'

@Middleware({ type: 'after' })
export class CustomErrorHandler implements ExpressErrorMiddlewareInterface {
  error(err: Error, _req: Request, res: Response): void {
    const json: { message: string; status: number; details?: string } = {
      status: 500,
      message: '',
    }
    if (
      err instanceof Array &&
      !err.filter((inst) => !(inst instanceof ValidationError)).length
    ) {
      json.status = 400
      json.message = err
        .map((inst) =>
          Object.keys(inst.constraints)
            .map((key) => inst.constraints[key])
            .join(', '),
        )
        .join('. ')
    } else {
      json.status = err['httpCode'] || json.status
      json.message = err['message'].toString()

      if (ENV !== ENVIRONMENT_PROD && json.status === 500) {
        json.details = err.stack
      }
    }

    res.status(json.status)
    res.json(json)

    console.log(err)
  }
}

@Middleware({ type: 'after' })
export class NotFoundErrorHandler implements ExpressMiddlewareInterface {
  use: RequestHandler = (_req, res) => {
    if (!res.statusMessage) {
      res.status(404).json({
        message: 'Page not found',
      })
    }
  }
}
