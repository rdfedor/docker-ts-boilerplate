import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers'
import { RequestHandler } from 'express'
import * as morgan from 'morgan'

@Middleware({ type: 'before' })
export class HttpLoggingHandler implements ExpressMiddlewareInterface {
  use: RequestHandler = (() => {
    //get real ip if passed by nginx
    morgan.token('remote-addr', function (req): string {
      return (
        req.headers['x-real-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress
      ).toString()
    })

    morgan.token('request-id', function (_req, res): string {
      return res.getHeader('x-request-id').toString()
    })

    return morgan(
      ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ":request-id"',
    )
  })()
}
