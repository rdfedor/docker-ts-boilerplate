import {
  IsUUID,
  IsNotEmpty,
  IsEmail,
  MinLength,
  MaxLength,
  IsJWT,
} from 'class-validator'

export class GetAccountRequestModel {
  @IsNotEmpty()
  @IsUUID('all')
  account_uuid: string
}

export class GetAccountByEmailRequestModel {
  @IsNotEmpty()
  @IsEmail()
  email: string
}

export class DeleteAccountRequestModel {
  @IsNotEmpty()
  @IsUUID('all')
  account_uuid: string
}

export class CreateAccountRequestModel {
  @IsNotEmpty()
  @MaxLength(100)
  firstName: string
  @IsNotEmpty()
  @MaxLength(100)
  lastName: string
  @IsJWT()
  @IsNotEmpty()
  token: string
  @IsNotEmpty()
  @MinLength(8)
  password: string
}

export class AccountRoleRequest {
  @IsNotEmpty()
  @IsUUID('all')
  public account_uuid: string

  @IsNotEmpty()
  public role_name: string
}
