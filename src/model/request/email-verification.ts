import {
  IsNotEmpty,
  IsEmail,
  MaxLength,
  IsUUID,
  IsJWT,
  IsNumberString,
} from 'class-validator'

export class SendEmailVerificationRequestToken {
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(254)
  public email: string

  @IsUUID('all')
  @IsNotEmpty()
  public account_uuid: string

  @IsNotEmpty()
  public challenge_code: string
}

export class SendEmailVerificationRequest {
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(254)
  public email: string
}

export class EmailVerificationRequest {
  @IsNotEmpty()
  @IsJWT()
  public token: string

  @IsNotEmpty()
  @IsNumberString()
  public challenge_code: string
}
