import * as Debug from 'debug'
import { Service } from 'typedi'
import * as Database from 'src/database/index'
import { NotFoundError } from 'src/error'
import {
  GetAccountRequestModel,
  DeleteAccountRequestModel,
  AccountRoleRequest,
  GetAccountByEmailRequestModel,
} from 'src/model/request/account'
import {
  SendPasswordResetRequest,
  ResetPasswordRequest,
} from 'src/model/request/password-reset'
import {
  SendEmailVerificationRequest,
  SendEmailVerificationRequestToken,
  EmailVerificationRequest,
} from 'src/model/request/email-verification'
import { validateOrReject } from 'class-validator'
import { ENV } from 'src/config'
import { ENVIRONMENT_DEV, ALL_ROLES } from 'src/constant'
import { JwtService } from './jwt'
import { MathService } from './math'
import { CryptService } from './crypt'
import { plainToClass } from 'class-transformer'
import { BadRequestError } from 'src/error'
import { AccountInterface, AccountQueryInterface } from 'src/interface/account'
import { RequestResult } from 'src/interface/request-result'

const debug = Debug('appservice:service:account')

@Service()
export default class AccountService {
  constructor(
    private db: Database.default,
    private jwtService: JwtService,
    private cryptService: CryptService,
    private mathService: MathService,
  ) {}

  public async getAccounts(): Promise<AccountInterface[]> {
    const result = await this.db.query('CALL getAccounts()')
    return result.data.map((row: AccountInterface) => {
      const newRow: AccountInterface = {
        ...row,
        ...{
          account_uuid: row.account_uuid.toString(),
        },
      }

      return newRow
    })
  }

  public async getAccount(
    req: GetAccountRequestModel,
  ): Promise<AccountInterface> {
    await validateOrReject(req)

    const result = await this.db.query('CALL getAccount(?)', [req.account_uuid])

    if (!result.data.length) {
      throw new NotFoundError('Account not found')
    }

    const [row] = <AccountQueryInterface[]>result.data

    return <AccountInterface>{
      ...row,
      ...{
        roles: !row.roles ? [] : row.roles.split(','),
        account_uuid: row.account_uuid.toString(),
      },
    }
  }

  public async getAccountByEmail(
    req: GetAccountByEmailRequestModel,
  ): Promise<AccountInterface> {
    await validateOrReject(req)

    const result = await this.db.query('CALL getAccountByEmail(?)', [req.email])

    if (!result.data.length) {
      throw new NotFoundError('Account not found')
    }

    const [row] = <AccountQueryInterface[]>result.data

    return <AccountInterface>{
      ...row,
      ...{
        roles: !row.roles ? [] : row.roles.split(','),
        account_uuid: row.account_uuid.toString(),
      },
    }
  }

  public async deleteAccount(
    req: DeleteAccountRequestModel,
  ): Promise<RequestResult> {
    await validateOrReject(req)

    const result = await this.db.query('CALL delAccount(?)', [req.account_uuid])

    if (!result.status.affectedRows) {
      throw new NotFoundError('Account not found')
    }

    return {
      status: 1,
    }
  }

  public async processSendPasswordResetRequest(
    req: SendPasswordResetRequest,
  ): Promise<string> {
    await validateOrReject(req)

    return this.jwtService.sign(
      {
        email: req.email,
      },
      {
        expiresIn: '2 days',
        audience: 'password-reset',
      },
    )
  }

  public async processResetPasswordRequest(
    req: ResetPasswordRequest,
  ): Promise<RequestResult> {
    await validateOrReject(req)

    const token = plainToClass(
      SendPasswordResetRequest,
      this.jwtService.verify(req.token, {
        audience: 'password-reset',
      }),
    )

    const encryptedPassword = await this.cryptService.hashText(req.password)

    const result = await this.db.query('CALL setAccountPassword(?, ?)', [
      token.email,
      encryptedPassword,
    ])

    if (!result.status.affectedRows) {
      throw new NotFoundError('Account not found')
    }

    return {
      status: 1,
    }
  }

  public async processSendEmailVerificationRequest(
    req: SendEmailVerificationRequest,
  ): Promise<string> {
    await validateOrReject(req)

    const challengeCode = new Array(6)
      .fill(1, 0, 6)
      .map(() => this.mathService.randomIntenger(0, 9).toString())
      .join('')

    if (ENV === ENVIRONMENT_DEV) {
      debug(`Challenge Code: ${challengeCode}`)
    }

    return this.jwtService.sign(
      {
        challenge_code: await this.cryptService.hashText(challengeCode),
        email: req.email,
      },
      {
        subject: 'verify-email',
      },
    )
  }

  public async processEmailVerificationRequest(
    req: EmailVerificationRequest,
  ): Promise<RequestResult> {
    await validateOrReject(req)

    const jwtRequest = plainToClass(
      SendEmailVerificationRequestToken,
      this.jwtService.verify(req.token, {
        subject: 'verify-email',
        maxAge: '24h',
      }),
    )

    const challengeCodeMatches = await this.cryptService.compare(
      req.challenge_code,
      jwtRequest.challenge_code,
    )

    if (!challengeCodeMatches) {
      throw new BadRequestError('Challenge code does not match')
    }

    const result = await this.db.query('CALL setAccountEmail(?, ?)', [
      jwtRequest.account_uuid,
      jwtRequest.email,
    ])

    if (!result.status.affectedRows) {
      throw new BadRequestError('Account email already verified')
    }

    return {
      status: 1,
    }
  }

  public async addAccountRole(req: AccountRoleRequest): Promise<RequestResult> {
    await validateOrReject(req)

    if (ALL_ROLES.indexOf(req.role_name) === -1) {
      throw new BadRequestError('Invalid role')
    }

    await this.db.query('CALL addAccountRole(?, ?)', [
      req.account_uuid,
      req.role_name,
    ])

    return {
      status: 1,
    }
  }

  public async delAccountRole(req: AccountRoleRequest): Promise<RequestResult> {
    await validateOrReject(req)

    if (ALL_ROLES.indexOf(req.role_name) === -1) {
      throw new BadRequestError('Invalid role')
    }

    await this.db.query('CALL delAccountRole(?, ?)', [
      req.account_uuid,
      req.role_name,
    ])

    return {
      status: 1,
    }
  }
}
