import * as Debug from 'debug'
import { Service } from 'typedi'
import * as Database from 'src/database/index'
import { NotFoundError, AccessDeniedError } from 'src/error'
import {
  CreateAccountRequestModel,
  GetAccountRequestModel,
  GetAccountByEmailRequestModel,
} from 'src/model/request/account'
import {
  AuthenticateUserRequest,
  RefreshTokenRequest,
} from 'src/model/request/auth'
import {
  SendPasswordResetRequest,
  ResetPasswordRequest,
} from 'src/model/request/password-reset'
import {
  SendEmailVerificationRequest,
  SendEmailVerificationRequestToken,
  EmailVerificationRequest,
} from 'src/model/request/email-verification'
import { validateOrReject } from 'class-validator'
import { ENV } from 'src/config'
import { ENVIRONMENT_DEV, ROLE_BANNED } from 'src/constant'
import { JwtService } from './jwt'
import { MathService } from './math'
import { CryptService } from './crypt'
import { plainToClass } from 'class-transformer'
import { BadRequestError } from 'routing-controllers'
import { AccountInterface } from 'src/interface/account'
import { LoginTokenInterface } from 'src/interface/login-token'
import { RequestResult } from 'src/interface/request-result'
import AccountService from './account'
import { JwtToken } from 'src/interface/jwt-request'
import { QueryResultInterface } from 'src/interface/query-result'
import { SqlError } from 'mariadb'

const debug = Debug('appservice:service:auth')

@Service()
export default class AuthService {
  constructor(
    private db: Database.default,
    private accountService: AccountService,
    private jwtService: JwtService,
    private cryptService: CryptService,
    private mathService: MathService,
  ) {}

  public async processRegisterAccount(
    req: CreateAccountRequestModel,
  ): Promise<LoginTokenInterface> {
    await validateOrReject(req)

    const jwtRequest = plainToClass(
      SendEmailVerificationRequestToken,
      this.jwtService.verify(req.token, {
        maxAge: '24h',
        subject: 'email-verification',
      }),
    )

    const encryptedPassword = await this.cryptService.hashText(req.password)

    const { firstName, lastName } = req
    const { email } = jwtRequest

    let record : QueryResultInterface = null;
    try {
    record = await this.db.query(
      'CALL addAccount(?, ?, ?, ?)',
      [firstName, lastName, encryptedPassword, email],
    )
    } catch (ex) {
      if (ex && ex instanceof SqlError && ex.code === 'ER_DUP_ENTRY') {
        throw new BadRequestError('Account already exists by that email')
      } else {
        console.log(ex)
        throw ex
      }
    }

    console.log(record.data)

    return this.getLoginToken({
      account_uuid: record.data['uuid'],
      first_name: firstName,
      last_name: lastName,
      password: encryptedPassword,
      email,
      roles: [],
      verified_email: true,
    })
  }

  public async processSendPasswordResetRequest(
    req: SendPasswordResetRequest,
  ): Promise<string> {
    await validateOrReject(req)

    return this.jwtService.sign(
      {
        email: req.email,
      },
      {
        expiresIn: '1h',
        audience: 'password-reset',
      },
    )
  }

  public async processResetPasswordRequest(
    req: ResetPasswordRequest,
  ): Promise<RequestResult> {
    await validateOrReject(req)

    const token = plainToClass(
      SendPasswordResetRequest,
      this.jwtService.verify(req.token, {
        audience: 'password-reset',
      }),
    )

    const encryptedPassword = await this.cryptService.hashText(req.password)

    await this.db.query('CALL setAccountPassword(?, ?)', [
      token.email,
      encryptedPassword,
    ])

    return {
      status: 1,
    }
  }

  public async processSendEmailVerificationRequest(
    req: SendEmailVerificationRequest,
  ): Promise<string> {
    await validateOrReject(req)

    const challengeCode = new Array(6)
      .fill(1, 0, 6)
      .map(() => this.mathService.randomIntenger(0, 9).toString())
      .join('')

    if (ENV === ENVIRONMENT_DEV) {
      debug(`Challenge Code: ${challengeCode}`)
    }

    return this.jwtService.sign(
      <SendEmailVerificationRequestToken>{
        challenge_code: await this.cryptService.hashText(challengeCode),
        email: req.email,
      },
      {
        subject: 'email-verification',
      },
    )
  }

  public async processEmailVerificationRequest(
    req: EmailVerificationRequest,
  ): Promise<RequestResult> {
    await validateOrReject(req)

    const jwtRequest = plainToClass(
      SendEmailVerificationRequestToken,
      this.jwtService.verify(req.token, {
        maxAge: '1h',
        subject: 'email-verification',
      }),
    )

    const challengeCodeMatches = await this.cryptService.compare(
      req.challenge_code,
      jwtRequest.challenge_code,
    )

    if (!challengeCodeMatches) {
      throw new BadRequestError('Challenge code does not match')
    }

    const result = await this.db.query('CALL setAccountEmail(?, ?)', [
      jwtRequest.account_uuid,
      jwtRequest.email,
    ])

    if (!result.status.affectedRows) {
      throw new BadRequestError('Account email already verified')
    }

    return {
      status: 1,
    }
  }

  public async processAuthenticateUserRequest(
    req: AuthenticateUserRequest,
  ): Promise<LoginTokenInterface> {
    await validateOrReject(req)

    const user = await this.accountService.getAccountByEmail(
      plainToClass(GetAccountByEmailRequestModel, {
        email: req.email,
      }),
    )

    if (!user.email) {
      throw new NotFoundError('Account not found')
    }

    const passwordMatches = await this.cryptService.compare(
      req.password,
      user.password,
    )

    if (!passwordMatches) {
      throw new AccessDeniedError('Invalid password')
    }

    return this.getLoginToken(user)
  }

  public async processRefreshToken(
    req: RefreshTokenRequest,
  ): Promise<LoginTokenInterface> {
    console.log(req.token)
    const token = <JwtToken>this.jwtService.verify(req.token, {
      maxAge: '2d',
    })
    const user = await this.accountService.getAccount(
      plainToClass(GetAccountRequestModel, {
        account_uuid: token.account_uuid,
      }),
    )

    return this.getLoginToken(user)
  }

  private getLoginToken(user: AccountInterface): LoginTokenInterface {
    if (user.roles.indexOf(ROLE_BANNED) > -1) {
      throw new AccessDeniedError('Account has been banned.')
    }

    return {
      token: this.jwtService.sign(
        {
          account_uuid: user.account_uuid.toString(),
          email: user.email,
          roles: user.roles,
        },
        // {
        //   expiresIn: JWT_TOKEN_DURATION,
        // },
      ),
      // expires_in: JWT_TOKEN_DURATION,
      iat: Math.floor(+new Date() / 1000),
      roles: user.roles,
      account_uuid: user.account_uuid.toString(),
      email: user.email,
      first_name: user.first_name,
      last_name: user.last_name,
      verified_email: user.verified_email,
    }
  }
}
