import { hash, compare } from 'bcrypt'
import { Service } from 'typedi'

@Service()
export class CryptService {

  public async hashText(text: string) : Promise<string> {
    return await hash(text, 10)
  }

  public async compare(text: string, hash: string) : Promise<boolean> {
    return await compare(text, hash)
  }
}
