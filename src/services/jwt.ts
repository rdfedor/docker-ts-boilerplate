import * as Debug from "debug";
import { Service } from "typedi";
import { sign, verify, SignOptions, VerifyOptions } from "jsonwebtoken";
import { JWT_PRIVATE_KEY, JWT_PUBLIC_KEY, JWT_TOKEN_ISSUER } from "src/config";
import { JWK, JWKS, JSONWebKeySet } from "jose";

const debug = Debug("appservice:service:jwt");

@Service()
export class JwtService {
  // private privateKey: JWK.RSAKey | JWK.ECKey | JWK.OKPKey | JWK.OctKey

  private keyStore: JWKS.KeyStore;

  private issuer: string;

  constructor() {
    const privateKey = JWK.asKey(
      Buffer.from(JWT_PRIVATE_KEY, "base64").toString("utf-8"),
      {
        alg: "RS256",
        use: "sig",
      }
    );

    const keyStore = new JWKS.KeyStore([privateKey]);

    keyStore.toJWKS();

    this.issuer = JWT_TOKEN_ISSUER;

    // this.privateKey = privateKey
    this.keyStore = keyStore;
  }
  public sign(
    // eslint-disable-next-line @typescript-eslint/ban-types
    payload: string | object | Buffer,
    options: SignOptions = {}
  ): string {
    debug(`Generating signed jwt ${JSON.stringify({ payload, options })}`);
    return sign(
      payload,
      Buffer.from(JWT_PRIVATE_KEY, "base64").toString("utf-8"),
      {
        ...{
          algorithm: "RS256",
          issuer: this.issuer,
        },
        ...options,
      }
    );
  }

  public verify(token: string, options: VerifyOptions = {}): string | unknown {
    const verifyOptions: VerifyOptions = {
      ...{
        algorithm: ["RS256"],
        issuer: this.issuer,
      },
      ...options,
    };
    debug(`Verifying signed jwt ${JSON.stringify({ token, options })}`);
    return verify(
      token,
      Buffer.from(JWT_PUBLIC_KEY, "base64").toString("utf-8"),
      verifyOptions
    );
  }

  public getJsonWebKeySet(): JSONWebKeySet {
    return this.keyStore.toJWKS();
  }
}

export default new JwtService();
