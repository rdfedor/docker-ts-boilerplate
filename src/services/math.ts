import { Service } from "typedi";

@Service()
export class MathService {
  public randomIntenger(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }
}

export default new MathService()
